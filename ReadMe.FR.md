English Version [here](ReadMe.md).  
URL du projet : [https://gitlab.com/tontonCD/textCodeToHtml]() 

PROJET
======
**textCodeToHtml** permet de convertir des fichiers de format .rtf ou .doc, en fichiers dans un format adapté à la publication Web.
Il trouve son utilité dans la publication de textes contenant des extraits de **code source** (programmation).
Les fichiers étant définis une fois pour toutes à l'issue de leur génération, la méthode est bien plus efficace que d'utiliser un plugin qui aurait fait la même chose à la demande.

Il génère les fichiers :

- html (.html) : peut être publié directement au moyen d'un client ftp, 
- Wordpress (.txt) : le logiciel crée d'une part un CSS (à insérer dans Appearance -> Theme Options), d'autre part le texte incluant les Tags CSS que wordpress saura interpréter, 




HOW TO
===============
1) créez un fichier .rtf ou .doc, saisissez du texte, copier/coller du code depuis Xcode,

2) Le texte "courant" est identifié comme étant de couleur noire (couleur #000000). Seules les parties de *couleurs* différentes seront testées comme parties de Code, aussi évitez de définir un style de code de couleur noire (@see notes sur la version #1.4)

3) Lancez le programme, ouvrez le fichier (avec le bouton "Open") ; chaque couleur reconnue est transformée en tag html, un fichier .html est généré ainsi qu'un fichier .txt pour Wordpress (gestion différente des retours à la ligne)



VERSION: #1.8
===============
rajout de la gestion des fontFaces : bold/italic,


VERSION: #1.6
===============
Lorsque un style est identifié, la ligne correspondante dans la liste est mise en évidence en temps réel,


VERSION: #1.5
===============
Les styles de couleurs appliquées au html incluent la valeur d'alpha (transparence).


VERSION: #1.4
===============
Les propriétés de Style sont lues directement depuis les préférences de Xcode. Testé pour Xcode 10.1.


VERSION : #1.3
===============
L'assignation d'un type de code à une couleur est encore codée en dur, il faut donc compiler le programme pour personnaliser cette fonction.
Ne vous préoccupez pas de la présentation peu soignée, parce que ce projet et aussi un exemple d'évolution de celle-ci.  

- FIX (fonctionnalités): implémentation de la fenêtre d'ouverture de fichier (le nom n'est plus codé en dur),
- FIX (fonctionnalités): résolution des problèmes d'affichage de la vue html et de la liste de tags,

Consultez CodeRtfConvert_log.rtf pour plus de détails sur les versions (s'affiche correctement depuis Xcode). 

A FAIRE
=======
- reconnaître les styles Gras et Italique,
- reconnaître les "notes" de texte, et les numéroter,
- reconnaître les commentaires (`/* .. */` -> `<!-- ... -->`) ?,
- générer séparément le .css et le .html,
- lire le .css du dossier contenant le fichier à traiter, s'il existe (actuellement les valeurs utilisées lors du parcours du fichier d'entrée sont codées "en dur"),
- proposer pour chaque style non connu, rencontré lors du parcours du fichier, de lui donner un nom de style,

EXEMPLE
=======
*(TODO)*

NOTE
====
Un outil standard assez pratique, en ligne de commande, est "textutil" avec l'option "-convert html", qui malheureusement ne tient pas compte du formatage de code.


---
Merci de consulter la page du projet : [http://chdfree.free.fr/wanmore/index.php/textcodetohtml/](http://chdfree.free.fr/wanmore/index.php/textcodetohtml/) 
