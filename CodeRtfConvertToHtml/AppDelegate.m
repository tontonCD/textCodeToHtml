//
//  AppDelegate.m
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 18/12/2014.
//  Copyright (c) 2014 Christophe. All rights reserved.
//

#import "AppDelegate.h"

#import "ColorComponents.h"
#import "WMPList.h"


@interface NSString(util)
-(NSString *) limit:(NSUInteger)len;
@end
@implementation NSString(util)
-(NSString *) limit:(NSUInteger)len {
    if([self length]<=len)
    {
        return self;
    } else
    {
        return [NSString stringWithFormat:@"%@...", [self substringToIndex:(len - 3)]];
    }
}
@end

@interface AppDelegate ()
{
    NSUInteger  index;
    NSString   *foundString;
    NSRange     lastSavedRange;  // for output file (html and txt)
    
    WebFrame    *aMainFrame;
}

@property (weak) IBOutlet NSWindow *window;


@end




// TODO: not used? FIXIT
//NSString * const tagBold =  @"   <strong>";
//NSString * const tagBold_ = @"   </strong>";
//NSString * const tagItal =  @"   <em>";
//NSString * const tagItal_ = @"   </em>";
//
//NSString * const tagSpan =  @"<span class=\"cppcode\">";   // old name: tagStart
//NSString * const tagSpan_ = @"</span>";                    // old name: tagEnd

//NSString * const tagBR =    @"<br />\r\n";   // note: the function appendData:dataUsingEncoding: adds "\n" again

NSStringEncoding encoding_OBSO = NSUTF8StringEncoding;


/* don't change this here ; look in the .pch file (command-click the kName) */


// PARSER
NSString *colorAttrName = @"NSColor";
NSString *fontAttrName = @"NSFont";
//NSString *fontAttr_Bold = fontNameAttr_Text_Bold; USELESS
//NSString *fontAttr_Ital = fontNameAttr_Text_Ital; USELESS


//
//NSColor *color_plainText;
//NSColor *color_comment;
//NSColor *color_string;
//NSColor *color_characters;
//NSColor *color_keywords;
////NSColor *color_typeNames
//NSColor *color_instanceVariables;
//NSColor *color_macros;
//NSColor *color_classNames;
//NSColor *color_function;
//NSColor *color_invite;
//











#if 1 // OK
typedef enum 
{
    idle = 0,
    parsing,
    paused,
    finished
} ParseState;
#else
typedef NS_ENUM(NSUInteger, ParseState)
{
    idle = 0,
    parsing,
    paused,
    finished
};
#endif










//#endif




NSMutableDictionary   *colorsDictionnary;

@implementation AppDelegate







/** log a string using a format string;
 * usage: replace NSLog(@"%@", @"ERROR ?");
* with wmLog(@"%@", @"ERROR ?"); */
static void wmLog(NSString *format, NSString *string) {
    NSString *temp = [NSString stringWithFormat:format, string];
    printf( "%s\n", [temp cStringUsingEncoding:NSMacOSRomanStringEncoding] );
}
//static void wmLogTrunc(NSString *format, NSString *string) {
//    if([string length]<=13)
//    {
//        
//    } else
//    {
//       string = [NSString stringWithFormat:@"%@...", [string substringToIndex:10]];
//    }
//    NSString *temp = [NSString stringWithFormat:format, string];
//    printf( "%s\n", [temp cStringUsingEncoding:NSMacOSRomanStringEncoding] );
//}

- (void)_updateRange   {
//    index += [foundString length];
//    lastSavedRange.location = index;
//    lastSavedRange.length = 0;
    
    lastSavedRange.location = index;
    lastSavedRange.length = [foundString length];
    index += lastSavedRange.length;
}

-(NSString *)cssStringWithStyleComponents:(StyleComponent *)component {
    int r = component->sRed   / 10; // in fact: /2550, then *255
    int v = component->sGreen / 10;
    int b = component->sBlue  / 10;
    int a = component->sAlpha / 10;
    float af = (float)a / 255.0;

    
    NSString *result_rgba = [NSString stringWithFormat:@"span.cppcode .%@ {\n\tcolor:rgba(%i, %i, %i, %.1f); }\n",
                        [component getEscapedStyleName],
                        r, v, b, af];
    return result_rgba;
}


- (void)parse
{
    
    static ParseState parseState = idle;

    parseState = /* ParseState. */ parsing;
    
    
    NSRange remainingMaxRange =  NSMakeRange(0, [stringFromFile length]);
    //NSRange scanRange = NSMakeRange(0, 1);
    

    NSColor *colorAttribute;
    NSFont  *fontAttribute;
    
    


    index = 0;
    lastSavedRange = NSMakeRange(0, 0); // for output file (html and txt)

    
    while(index < [stringFromFile length])
    {
        remainingMaxRange =  NSMakeRange(index, [stringFromFile length]-index);
        
        // look for CRLF
        NSRange nextCrlf = [stringFromFile_String rangeOfString:@"\n" options:NSLiteralSearch range:remainingMaxRange];
        // and for @"\n"? -> never, the file is converted before we parse it
        
        
        if(nextCrlf.location == index)
        {
            /* CRLF */
            
            NSRange currentRange = NSMakeRange(index, 1);
            
            

#if OLD
            [aHtmlData insertHtml:tagBR]; // CHECK: it may fail to be added twice (if currentRange.length>1)
            
            //[aWPreData appendData:[@"\r\n"        dataUsingEncoding:encoding_OBSO]]; NO, was appended because not filtered
#else
            [aHtmlData insertBR]; // CHECK: it may fail to be added twice (if currentRange.length>1)
#endif
            // update: get
            NSRange testRange = currentRange;
            while(true) {
                testRange.location++;
                if(testRange.location>=stringFromFile_String.length) // FIX
                {
                    break;
                }
                NSString *testString = [stringFromFile_String substringWithRange:testRange];
                if([testString isEqualToString:@"\n"])    {
                    currentRange.length++;
                    [aHtmlData insertBR]; // TODO: optimize: replace with [aHtmlData insertBR:foundString.length]
                } else
                    break;
            }
            wmLog(@"Found string in %@: (new line)", NSStringFromRange(currentRange));
            // update Range
            foundString = [stringFromFile_String substringWithRange:currentRange];
            [self _updateRange];

            // TODO: update UI
            continue;
        }

        
        /* look for attribute changes within remainingMaxRange */
        NSRange longestColorEffRrange;
        NSRange longestFontEffRrange;
        

            
        // these update longestColorEffRrange and longestFontEffRrange
        colorAttribute = [stringFromFile attribute:colorAttrName atIndex:index longestEffectiveRange:&longestColorEffRrange inRange:remainingMaxRange];
        fontAttribute = [stringFromFile attribute:fontAttrName atIndex:index longestEffectiveRange:&longestFontEffRrange inRange:remainingMaxRange];
        // longestEffectiveRange: "if found, the full range over which the value of the named attribute is the same as that at index, clipped to rangeLimit."
        
        
        /* take the largest range for this font/color */
        NSUInteger nextColorChange = NSMaxRange(longestColorEffRrange);
        NSUInteger nextFontChange = NSMaxRange(longestFontEffRrange);
        // take the less of two:
        NSUInteger nextChange = (nextFontChange < nextColorChange) ? nextFontChange : nextColorChange;
        
        // stop the range at CRLF, if any:
        if( nextChange >= NSMaxRange(nextCrlf) )
            nextChange =  NSMaxRange(nextCrlf)-1;
        
        NSRange currentRange = NSMakeRange(index, nextChange-index);
        foundString = [stringFromFile_String substringWithRange:currentRange];
#if DEBUG
        wmLog(@"Found string in %@",
              [NSString stringWithFormat:@"%@: \"%@\"", NSStringFromRange(currentRange), [foundString limit:20]]);
#endif

        
        if( fontAttribute==nil )
        {
            wmLog(@"%@", @"ERROR ?");
            break;
        }
        
        wmLog(@" Font :%@", fontAttribute.fontName);
        
        
        [self.aTextView setSelectedRange:currentRange];                         // was: ":longestColorEffRrange"
        [self.aTextView scrollRangeToVisible:currentRange];
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.05]]; // requiered

        
        if( colorAttribute==nil )
        {

            // it can be spaces ?
            
//#if 0
//            NSString *spaces = [[stringFromFile string] substringWithRange:longestColorEffRrange];
//            // TODO: VS currentRange?
//
//            [aHtmlData insertHtml:tagSpan    ];
//            [aHtmlData insertHtml:spaces ];
//            [aHtmlData insertHtml:tagSpan_    ];
//
//            // TODO:
//            [aWPreData appendData:[tagSpan    dataUsingEncoding:encoding_OBSO]];
//            // wrong:
//            //[aHtmlData insertHtml:spaces];
//            // TODO:
//            [aWPreData appendData:[tagSpan_    dataUsingEncoding:encoding_OBSO]];
//
//#else
            // TODO: check (this is not code) if bold (not handled)
            
#if OLD
            NSString *stringToInsert = [NSString stringWithFormat:@"%@%@%@",
                                        tagSpan,  foundString,  tagSpan_   ];
            [aHtmlData insertHtml:stringToInsert    ];
#else
            //[aHtmlData insertCode:foundString    ];
            // TODO: check is <span> usefull -> [aHtmlData insertHtml:foundString    ];
            
        //TODO: chech bold/italic
            /*
             (lldb) po fontAttribute.italicAngle    -11

             (lldb) po fontAttribute.fixedPitch      NO
             
             (lldb) po fontAttribute.xHeight         5.15625
             
             (lldb) po fontAttribute.italicAngle            0
             (lldb) po fontAttribute.xHeight                5.10791015625
             (lldb) po fontAttribute.isFixedPitch           NO
             (lldb) po fontAttribute.capHeight              6.9501953125
             
             
             (lldb) po [fontAttribute.fontDescriptor objectForKey:NSFontFaceAttribute]
             Regular
             
             (lldb) po [fontAttribute.fontDescriptor objectForKey:NSFontTraitsAttribute]
             {
             NSCTFontProportionTrait = 0;
             NSCTFontSlantTrait = 0;
             NSCTFontSymbolicTrait = 2147483648;
             NSCTFontWeightTrait = 0;
             }
             
             (lldb) po [fontAttribute.fontDescriptor objectForKey:NSFontFaceAttribute]
             Bold
             */
            
            NSString *faceAttribute = [fontAttribute.fontDescriptor objectForKey:NSFontFaceAttribute];
//            BOOL isBold = [faceAttribute isEqualToString:@"Bold"];
//            if(        [faceAttribute isEqualToString:@"Regular"])      {
//                [aHtmlData insertHtml:foundString    ];
//            } else if( [faceAttribute isEqualToString:@"Bold"])         {
//                [aHtmlData insertHtml:foundString    ];
//            } else if( [faceAttribute isEqualToString:@"Bold Italic"])  {
//                [aHtmlData insertHtml:foundString    ];
//            } else if( [faceAttribute isEqualToString:@"Italic"])       {
//                [aHtmlData insertHtml:foundString    ];
//            }  else                                                     {
//                [aHtmlData insertHtml:foundString    ];
//            }
             [aHtmlData insertHtml:foundString withAttributes:faceAttribute];
#endif

            
//#endif
            /* adjust attributes in UI */
            [self.aTextView setFont:     fontAttribute  range:currentRange];    // was: ":longestFontEffRrange"

            
            /* update the WebView */
            [aMainFrame loadData:[aHtmlData html] MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:NULL];
            [_aWebView setNeedsDisplay:YES];
            //TEST
//            NSView *testView = mainFrame.frameView.subviews.firstObject;
//            if( testView && [testView isKindOfClass:[NSScrollView class]] ) {
//                NSScrollView *scrollView = (NSScrollView *)testView;
//
//            }
            
            
            // for DEBUG (make the WebView displays)
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
            
            // data was saved, so update lastSavedRange: // TODO: report
            [self _updateRange];
            
            // TODO: don't close  "<span>"

            continue; // TODO: replace with 'else'
        }
        
        /* adjust attributes in UI */
        [self.aTextView setTextColor:colorAttribute range:currentRange];    // was: ":longestColorEffRrange"
        [self.aTextView setFont:     fontAttribute  range:currentRange];    // was: ":longestFontEffRrange"
//        // DISPLACED
//        [self.aTextView setSelectedRange:currentRange];                         // was: ":longestColorEffRrange"
//        [self.aTextView scrollRangeToVisible:currentRange];
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];

                
                

        StyleComponent *currentStyle = [[StyleComponent alloc] initWithColor:colorAttribute andFont:fontAttribute];
        StyleComponent *foundExistingStyle = [ColorComponents findStyleComponentFromStyle:currentStyle];
        
        
        BOOL styleExists = ( foundExistingStyle != nil );
        if(styleExists)    {
            
            // found a "code" style
            
            /* * save as Code * */
            
            // TODO: factorize
            
            foundExistingStyle->sUsed += 1;
            
            /* 1) HTML */
            // optimise: build the whole tag before insert
#if OLD
            NSString *tagSpan2 = [NSString stringWithFormat:@"<span class=\"%@\">", [foundExistingStyle getEscapedStyleName]];
           NSString *stringToInsert = [NSString stringWithFormat:@"%@%@%@%@%@",
                                        tagSpan, tagSpan2,  foundString,  tagSpan_, tagSpan_   ];
            [aHtmlData insertHtml:stringToInsert];
#else
            [aHtmlData insertCode:foundString withClass:[foundExistingStyle getEscapedStyleName]];
#endif
            // TODO: refactor and optimise
                            


                            
            /* 2) WordPress */
//            [aWPreData appendData:[stringToInsert dataUsingEncoding:encoding_OBSO]];
            // -> done (now factorized)
            
            /* selection */
            
            [self _updateRange];
                            
            
//            /* update the WebView */
//            // DISPLACED
//            //WebFrame *mainFrame = [_aWebView mainFrame];
//            [aMainFrame loadData:[aHtmlData html] MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:NULL];
//            [_aWebView setNeedsDisplay:YES]; // TODO: report for main text
//            // TODO: set Scroll
            
            // DISPLACED
            [_aAttributesTableView selectRowIndexes:
             [NSIndexSet indexSetWithIndex:foundExistingStyle->sStyleIndex] byExtendingSelection:NO];
            [_aAttributesTableView scrollRowToVisible:foundExistingStyle->sStyleIndex];
            
//            // for DEBUG (make the WebView displays)
//            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
            

                        
                        
                        //[[self window] setViewsNeedDisplay:YES];
 //                       [[self window] update];
                        //[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
                        
                    
                    
        } // end: if(foundExistingStyle)
        else {
            
            /* not Code, Plain text */ // SEEMS WRONG (never reached)
            
            /* save until here */
            NSRange aRange;
            aRange.location = lastSavedRange.location + lastSavedRange.length;
            aRange.length = index - aRange.location;
                    
            // seems USELESS:
            NSString *passedString = [stringFromFile_String  substringWithRange:aRange];
             if(passedString.length > 0) {
                [aHtmlData insertHtml:passedString];
                
                index += passedString.length;
            }

            // TODO: tester  (fontAttribute.isFixedPitch)
            if(fontAttribute.isFixedPitch)  {
                // ?
            }

                    
//#if OLD (but REF)
//                    [aHtmlData appendData:[foundString dataUsingEncoding:encoding]];
//                    [aWPreData appendData:[foundString dataUsingEncoding:encoding]];
//#else
                    [aHtmlData insertHtml:foundString];
//#endif
                    
//                    lastSavedRange.location = index + [foundString length];
//                    lastSavedRange.length = 0;
            [self _updateRange];
        }
        
        /* update the WebView */
        //WebFrame *mainFrame = [_aWebView mainFrame];
        [aMainFrame loadData:[aHtmlData html] MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:NULL];
        [_aWebView setNeedsDisplay:YES]; // TODO: report for main text
        // TODO: set Scroll

        /* make the WebView displays (supposing it did update) */
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.05]];
        
        
        
        if(parseState == paused)
            return;
        
    } // end: while
    
    return;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

    [ColorComponents initFromPrefs];
}



-(void)_startParse
{
    [_aAttributesTableView reloadData];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]]; // requiered
    
 
    aHtmlData = [[WMHTMLData alloc] init];
//    aWPreData = [[NSMutableData alloc] init];
    

    /* fill the CSS part */
    for(StyleComponent *styleComponent in [ColorComponents stylesArray])   {

        [aHtmlData insertCss:[self cssStringWithStyleComponents:styleComponent]];
    }


    

    

    
    /* set the string from file to the top TextView; important: the text displays for now with NO attribute */
    [self.aTextView setString:stringFromFile_String];
    
    // TEST: set the WebFrame now, then use insert instead of further loadData
    aMainFrame = [_aWebView mainFrame];
    [aMainFrame loadData:[aHtmlData html] MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:NULL];
    // TEST
    //[_aWebView replaceSelectionWithMarkupString:@"Hello <b>Word</b>"];
    //- (BOOL)searchFor:(NSString *)string direction:(BOOL)forward caseSensitive:(BOOL)caseFlag wrap:(BOOL)wrapFlag;
    
    [_aWebView setNeedsDisplay:YES];
    
    // make the display occurs
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];

    
    
    // TODO: now, tags ends are added after parse
    [self parse];

    
    // @see https://code.google.com/p/google-toolbox-for-mac/source/browse/trunk/Foundation/GTMNSString%2BHTML.h
    //    -> https://github.com/google/google-toolbox-for-mac/blob/master/Foundation/GTMNSString%2BHTML.m
    // @see https://code.google.com/p/google-toolbox-for-mac/source/browse/trunk/Foundation/GTMNSString%2BXML.h

    

    
    /* update the WebView */
    //WebFrame *mainFrame = [_aWebView mainFrame];
    [aMainFrame loadData:[aHtmlData html] MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:NULL];
    [_aWebView setNeedsDisplay:YES];
    /* TODO: deprecated, use the WKWebView */
    
}

-(IBAction)openAction:(id)sender
{
    NSURL *url = 0;
    
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setAllowsMultipleSelection:NO];               // default
    
    NSArray *fileTypes = [NSArray arrayWithObjects:@"rtf", @"doc", nil];
    [openPanel setAllowedFileTypes:fileTypes];
    
    NSInteger choosed = [openPanel runModal];
    
# if DEBUG
    // make the panel desapears ; repeat (within lldb) if necessary
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
# endif
    
    
    if(choosed==NSFileHandlingPanelOKButton)
    {
        NSArray *urls = [openPanel URLs];
        url = [urls objectAtIndex:0];
    }
    if(url)
    {
        /* REF:
         aFileNamePath_Input =  @"/Users/Administrateur/wanmore/WEB/Article08_Downloads_textCodeToHtml.rtf";
         aFileNameString_Input =  @"Article08_Downloads_textCodeToHtml.rtf";
         
         aFileNameString_Html = @"Article08_Downloads_textCodeToHtml.html";
         aFileNameString_WordPress = @"Article08_Downloads_textCodeToHtml.txt";
         aFileNamePath_Html = @"/Users/Administrateur/wanmore/WEB/Article08_Downloads_textCodeToHtml.html"; // was aFileOutPath
         aFileNamePath_WordPress = @"/Users/Administrateur/wanmore/WEB/Article08_Downloads_textCodeToHtml.txt";
         */
        
        aFileNamePath_Input =   [url path];          //  @"/Users/Administrateur/wanmore/WEB/Article10_BUG.rtf"
        aFileNameString_Input = [((NSArray*)[aFileNamePath_Input componentsSeparatedByString:@"/"]) lastObject]; // Article10_BUG.rtf
        //NSURL *baseUrl = [url baseURL]; // nil
        
        /* replace ".rtf" with {".html", .txt", } ; make it compatible with ".doc" and further allowed extentions */
        NSString *extention = [aFileNameString_Input pathExtension]; // -> e.g. "rtf" (not ".rtf")
        // 1) file name
        NSRange extentionRange = NSMakeRange([aFileNameString_Input length] - [extention length], [extention length]);
        aFileNameString_Html =      [aFileNameString_Input stringByReplacingCharactersInRange:extentionRange withString:@"html"];   // => "name.html"
        extentionRange.location -=1;
        extentionRange.length +=1;
        aFileNameString_WordPress = [aFileNameString_Input stringByReplacingCharactersInRange:extentionRange withString:@"_wp.txt"];// => "name_wp.txt"
        // 2) file path
        extentionRange = NSMakeRange([aFileNamePath_Input length] - [extention length], [extention length]);
        aFileNamePath_Html =        [aFileNamePath_Input stringByReplacingCharactersInRange:extentionRange withString:@"html"];
        extentionRange.location -=1;
        extentionRange.length +=1;
        aFileNamePath_WordPress =   [aFileNamePath_Input stringByReplacingCharactersInRange:extentionRange withString:@"_wp.txt"];
        // UI:
        [self.aFileNameTextField_Input     setStringValue:aFileNameString_Input];
        [self.aFileNameTextField_Html      setStringValue:aFileNameString_Html];
        [self.aFileNameTextField_WordPress setStringValue:aFileNameString_WordPress];
        // (lldb) po     [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]]
        
        
        /* shop data */
        fileInData = [NSData dataWithContentsOfFile:aFileNamePath_Input];
        // ???:  [NSString stringWithContentsOfFile:@"/usr/share/dict/words"];
        if(fileInData==0)
            return;
        stringFromFile = [[NSAttributedString alloc] initWithRTF:fileInData documentAttributes:NULL];
        stringFromFile_String = [stringFromFile string];
        
        /* GO */
        [self _startParse];
        
        // if(noError)  { ... }
        [self.aSaveHtmlButton setEnabled:YES];
        [self.aSaveWPreButton setEnabled:YES];
    }
    
    
    //return url;
}
-(IBAction)saveHtmlAction:(id)sender
{
    //[aHtmlData finalize]; ?
    [aHtmlData writeHTMLToFile:aFileNamePath_Html      atomically:false];
    
    [self.aSaveHtmlButton setEnabled:NO];
}

-(IBAction)saveWPreAction:(id)sender
{
    //[aWPreData writeToFile:aFileNamePath_WordPress atomically:false];
    [aHtmlData writeWPToFile:aFileNamePath_WordPress atomically:false];
    
    [self.aSaveWPreButton setEnabled:NO];
}



#pragma mark- NSTableViewDelegate for _aAttributes

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSInteger result = [ColorComponents stylesArray].count;
    return result;
}

    




- (nullable id)tableView:(NSTableView *)tableView objectValueForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString *result;
//    NSAttributedString *resultWithAtt = nil;

    StyleComponent *theComponentObject = [ColorComponents stylesArray][row];
    
    switch ([[tableColumn identifier] integerValue]) {
        case 1: // name
            result = [[NSString alloc] initWithFormat:@"%@", [theComponentObject getStyleName]];
            break;
            
        case 2: // r, v, b
            result = [theComponentObject getHexString];
//            NSDictionary *temp = @{colorAttrName: [NSColor colo
//            resultWithAtt = [NSAttributedString alloc] initWithString:result attributes:@NSDictionary
            // produces (e.g.): @"#007400"
            break;
            
        default:
            result = @"error";
    }

#if 0 // DEBUG
    wmLog(@"row: %@ ", [NSString stringWithFormat:@"%li -> %@", row, result] );
#endif
    
    return result; // use attributedString with the color ?
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    NSTableView *theTableView = [notification object];
    NSInteger theRow = [theTableView selectedRow]; // starts at 0, -1 if none
}

#pragma mark-

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

// NSTextViewDelegate
- (void)textViewDidChangeSelection:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSRange range = [((NSValue *)[userInfo objectForKey:@"NSOldSelectedCharacterRange"]) rangeValue];
    if( NSEqualRanges(range, NSMakeRange(0, 0)) )   {
        return;
    }
    if( range.length == 0 )   {
        return;
    }

}

@end
