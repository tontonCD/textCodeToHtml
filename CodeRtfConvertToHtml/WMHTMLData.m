//
//  WMHTMLData.m
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 28/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

#import "WMHTMLData.h"



/* don't change this */
NSString * const tagHTML =  @"<HTML>\n";
NSString * const tagBody =  @" <BODY>\n";
NSString * const tagHead =  @"  <HEAD>\n";
NSString * const tagMeta =  @"   <meta charset=utf-8 />\n";
NSString * const tagStyle = @"   <STYLE>\n";

NSString * const tagStyle_ = @"   </STYLE>\n";
NSString * const tagHead_ =  @"  </HEAD>\n";
NSString * const tagBody_ =  @" </BODY>\n";
NSString * const tagHTML_ =  @"</HTML>\n";

// TODO: not used? FIXIT
NSString * const tagBold =  @"   <strong>";
NSString * const tagBold_ = @"   </strong>";
NSString * const tagItal =  @"   <em>";
NSString * const tagItal_ = @"   </em>";


NSString * const tagSpan =  @"<span class=\"cppcode\">";   // old name: tagStart
NSString * const tagSpan_ = @"</span>";                    // old name: tagEnd


NSString * const tagBR =    @"<br />\r\n";   // note: the function appendData:dataUsingEncoding: adds "\n" again


NSStringEncoding encoding = NSUTF8StringEncoding;

@interface WMHTMLData()
{
    NSMutableData *wHtmlData;                       // data for .html file
    NSUInteger     wHtmlData_currentPosition_css;   // for insertion
    NSUInteger     wHtmlData_currentPosition_html;  // for insertion

    NSMutableData *wWPData;                       // data for .txt file (Wordpress)
    NSUInteger     wWPData_currentPosition_css;   // for insertion
    NSUInteger     wWPData_currentPosition_html;  // for insertion
}
@end


@implementation WMHTMLData

-(void)_initDataStart:(NSMutableData *)wichData  {
    
    [wichData appendData:[tagHTML dataUsingEncoding:encoding]];
    [wichData appendData:[tagHead dataUsingEncoding:encoding]];
    [wichData appendData:[tagMeta dataUsingEncoding:encoding]];
    [wichData appendData:[tagStyle dataUsingEncoding:encoding]];

     // TODO: WRONG, use prefs
    [wichData appendData:[@".cppcode    {\n\
                           font-family: \"Lucida Console\", \"Bitstream Vera Sans Mono\", \"Courier New\", Courier, monospace !important;\n\
                           font-size: 1.0em; /* white-space:pre; */ color: black; }\n" dataUsingEncoding:encoding]];
}
-(void)_initDataEnd:(NSMutableData *)wichData  {
    // 1) html:
    // close tags: </STYLE>
    [wHtmlData appendData:[tagStyle_ dataUsingEncoding:encoding]];
    // open tags:    <BODY>
    [wHtmlData appendData:[tagBody dataUsingEncoding:encoding]];
    
    wHtmlData_currentPosition_html = [wHtmlData length];
    
    [wHtmlData appendData:[tagBody_ dataUsingEncoding:encoding]];
    [wHtmlData appendData:[tagHTML_ dataUsingEncoding:encoding]];

    // 2) WP:
    // close tags: </STYLE>
    [wWPData appendData:[tagStyle_ dataUsingEncoding:encoding]];
    // open tags:    <BODY>
    [wWPData appendData:[tagBody dataUsingEncoding:encoding]];
    
    wWPData_currentPosition_html = [wWPData length];
    
    [wWPData appendData:[tagBody_ dataUsingEncoding:encoding]];
    [wWPData appendData:[tagHTML_ dataUsingEncoding:encoding]];
}


-(instancetype)init
{
    //NSAttributedString *temp2 = [tempS stringByEncodingHTMLEntities];
    
    self = [super init];
    if(self)
    {
        wHtmlData = [[NSMutableData alloc] init];
        wWPData =   [[NSMutableData alloc] init];

    
    /* INIT htmlData */
#if 0
    [wHtmlData appendData:[tagHTML dataUsingEncoding:encoding]];
    [wHtmlData appendData:[tagHead dataUsingEncoding:encoding]];
    [wHtmlData appendData:[tagMeta dataUsingEncoding:encoding]];
    [wHtmlData appendData:[tagStyle dataUsingEncoding:encoding]];
    
    //[htmlData appendData:[@".cppcode {    }\n" dataUsingEncoding:encoding]];
        // TODO: WRONG
    [wHtmlData appendData:[@".cppcode    {\n\
                           font-family: \"Lucida Console\", \"Bitstream Vera Sans Mono\", \"Courier New\", Courier, monospace !important;\n\
                           font-size: 1.0em; /* white-space:pre; */ color: black; }\n" dataUsingEncoding:encoding]];
#else
        [self _initDataStart:wHtmlData];
        [self _initDataStart:wWPData];
#endif
        
        wHtmlData_currentPosition_css = [wHtmlData length];
        wWPData_currentPosition_css = [wWPData length]; // useless (?)


#if 0
    // close tags: </STYLE>
    [wHtmlData appendData:[tagStyle_ dataUsingEncoding:encoding]];
    // open tags:    <BODY>
    [wHtmlData appendData:[tagBody dataUsingEncoding:encoding]];

    wHtmlData_currentPosition_html = [wHtmlData length];
    
    [wHtmlData appendData:[tagBody_ dataUsingEncoding:encoding]];
    [wHtmlData appendData:[tagHTML_ dataUsingEncoding:encoding]];
#else
        [self _initDataEnd:wHtmlData];
        [self _initDataEnd:wWPData];
#endif
    }
    return self;
}

/** inserts the param in the CSS part */
//- (void)_insertBytesInHtmlAtPos:(NSUInteger)location withBytes:(const void *)buffer length:(NSUInteger)length {
//    
//}
- (void)insertCss:(NSString *)cssString      {

    const char    *cssUtfString = [cssString UTF8String];
    NSMutableData *cssData = [NSMutableData dataWithBytes:cssUtfString length:strlen(cssUtfString)];
    NSUInteger     cssLen = [cssData length];
    unsigned char *cssBuffer = malloc(cssLen);
    [cssData getBytes:cssBuffer length:cssLen];
    
    
    //unsigned char *htmlBytes = [htmlString dataUsingEncoding:encoding].bytes;
    [wHtmlData replaceBytesInRange:NSMakeRange(wHtmlData_currentPosition_css, 0) withBytes:cssBuffer length:cssLen];
    wHtmlData_currentPosition_css += cssLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]

    [wWPData replaceBytesInRange:NSMakeRange(wWPData_currentPosition_css, 0) withBytes:cssBuffer length:cssLen];
    wWPData_currentPosition_css += cssLen; // p [[NSString alloc] initWithData:wWPData encoding:encoding]

    
    
    
    // and also:
    wHtmlData_currentPosition_html += cssLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
    wWPData_currentPosition_html += cssLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
    // may be useless for wWPData
}

/** inserts the param in the BODY part */
- (void)insertHtml:(NSString *)htmlString    {
    //[htmlData appendData:[htmlString dataUsingEncoding:encoding]];
    
    const char    *htmlUtfString = [htmlString UTF8String];
    NSMutableData *htmlData = [NSMutableData dataWithBytes:htmlUtfString length:strlen(htmlUtfString)];
    NSUInteger     htmlLen = [htmlData length];
    unsigned char *htmlBuffer = malloc(htmlLen);
    [htmlData getBytes:htmlBuffer length:htmlLen];
    
    
    //unsigned char *htmlBytes = [htmlString dataUsingEncoding:encoding].bytes;
    [wHtmlData replaceBytesInRange:NSMakeRange(wHtmlData_currentPosition_html, 0) withBytes:htmlBuffer length:htmlLen];
    wHtmlData_currentPosition_html += htmlLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
    
    /* WP:  tag/span are the same for html and WP */
    
    [wWPData replaceBytesInRange:NSMakeRange(wWPData_currentPosition_html, 0) withBytes:htmlBuffer length:htmlLen];
    wWPData_currentPosition_html += htmlLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
    
    free(htmlBuffer);
    htmlBuffer = 0;
}
- (void)insertHtml:(NSString *)htmlString withAttributes:(NSString *)fontFaceAttributes {
    BOOL isBold =   [fontFaceAttributes containsString:@"Bold"];
    BOOL isItalic = [fontFaceAttributes containsString:@"Italic"];

    /*
    if(        [faceAttribute isEqualToString:@"Regular"])      {
        [aHtmlData insertHtml:foundString    ];
    } else if( [faceAttribute isEqualToString:@"Bold"])         {
        [aHtmlData insertHtml:foundString    ];
    } else if( [faceAttribute isEqualToString:@"Bold Italic"])  {
        [aHtmlData insertHtml:foundString    ];
    } else if( [faceAttribute isEqualToString:@"Italic"])       {
        [aHtmlData insertHtml:foundString    ];
    }  else                                                     {
        [aHtmlData insertHtml:foundString    ];
    }
     */
    NSString *finalString = htmlString;
    if(isBold)      {
        finalString = [[tagBold stringByAppendingString:finalString] stringByAppendingString:tagBold_];
    }
    if(isItalic)    {
        finalString = [[tagItal stringByAppendingString:finalString] stringByAppendingString:tagItal_];
    }
    [self insertHtml:finalString];

}
- (void)insertCode:(NSString *)htmlString   {
    NSString *stringToInsert = [NSString stringWithFormat:@"%@%@%@", tagSpan,  htmlString,  tagSpan_   ];
    [self insertHtml:stringToInsert];
}

- (void)insertCode:(NSString *)htmlString withClass:(NSString *)classIdentifier   {
    NSString *tagSpan2 = [NSString stringWithFormat:@"<span class=\"%@\">", classIdentifier];
    
    NSString *stringToInsert = [NSString stringWithFormat:@"%@%@%@%@%@",
                                tagSpan, tagSpan2,  htmlString,  tagSpan_, tagSpan_   ];
    [self insertHtml:stringToInsert];
}




- (void)_insertBR_html    {
    const char    *htmlUtfString = [tagBR UTF8String];  // @"<br />\r\n"
    NSMutableData *htmlData = [NSMutableData dataWithBytes:htmlUtfString length:strlen(htmlUtfString)];
    NSUInteger     htmlLen = [htmlData length];
    unsigned char *htmlBuffer = malloc(htmlLen);
    [htmlData getBytes:htmlBuffer length:htmlLen];
    
    
    //unsigned char *htmlBytes = [htmlString dataUsingEncoding:encoding].bytes;
    [wHtmlData replaceBytesInRange:NSMakeRange(wHtmlData_currentPosition_html, 0) withBytes:htmlBuffer length:htmlLen];
    wHtmlData_currentPosition_html += htmlLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
}
 - (void)_insertBR_WP    {
    const char    *wpUtfString = [@"\r\n" UTF8String];
    NSMutableData *wpData = [NSMutableData dataWithBytes:wpUtfString length:strlen(wpUtfString)];
    NSUInteger     wpLen = [wpData length];
    unsigned char *wpBuffer = malloc(wpLen);
    [wpData getBytes:wpBuffer length:wpLen];
    
    
    //unsigned char *wpBytes = [wpString dataUsingEncoding:encoding].bytes;
    [wWPData replaceBytesInRange:NSMakeRange(wWPData_currentPosition_html, 0) withBytes:wpBuffer length:wpLen];
    wWPData_currentPosition_html += wpLen; // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
}
- (void)insertBR    {
    [self _insertBR_html];
    [self _insertBR_WP];
}
    
    
- (NSData *)html    {
    return wHtmlData;
    // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
}
- (NSData *)wp    {
    return wHtmlData;
    // p [[NSString alloc] initWithData:wHtmlData encoding:encoding]
}


- (BOOL)writeHTMLToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile  {
    BOOL result = [wHtmlData writeToFile:path atomically:useAuxiliaryFile];
    return result;
}
- (BOOL)writeWPToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile  {
    BOOL result = [wWPData writeToFile:path atomically:useAuxiliaryFile];
    return result;
}


@end
