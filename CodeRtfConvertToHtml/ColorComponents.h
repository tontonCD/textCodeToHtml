//
//  ColorComponents.h
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 19/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface StyleComponent : NSObject // was: IntColorValue
{
    NSString *sShortStyleName;
    NSString *sShortStyleName_escaped;

@public

    // color:
    int sRed, sGreen, sBlue, sAlpha;
    
    // font:
    NSString *fontName, *fontSize;
    CGFloat   fontPtSize;
    
    NSUInteger  sUsed; /**< counts occurences for this Style in html, i.e. references to CSS */
    NSUInteger  sStyleIndex;
}

///-(instancetype)initWithColor:(NSColor *)color;
-(instancetype)initWithColorString:(NSString *)color andFontString:(NSString *)font;
-(instancetype)initWithColor:(NSColor *)color andFont:(NSFont *)font;

-(NSString *)setStyleName:(NSString *)styleName;

-(NSString *)getHexString;          /**< returns: a string as @"#AABBCCDD", i.e. RGB + Aplha */
-(NSString *)getStyleName;          /**< returns the style name (e.g. "identifier.variable"), only used for TableView */
-(NSString *)getEscapedStyleName;   /**< returns the escaped style name (e.g. "identifier.variable" -> "identifier_variable") */

-(NSString *)description;
-(BOOL)isEqual:(StyleComponent *)object;

@end



@interface ColorComponents : NSObject // OBSO ?
{
@public
    NSString    *name;
    NSColor     *components; // r, v, b;
    NSUInteger hash;
    
    BOOL    foundInText;
    
}
-(instancetype)init;
-(instancetype)initWithName:(NSString *)name andColor:(NSColor *)color;
-(instancetype)initWithFont:(NSFont *)font andColor:(NSColor *)color;

+ (NSMutableArray<StyleComponent *> *)stylesArray;

+(void)initFromPrefs;


+(StyleComponent *)findStyleComponentFromStyle:(StyleComponent *)styleComponent;

@end



NS_ASSUME_NONNULL_END
