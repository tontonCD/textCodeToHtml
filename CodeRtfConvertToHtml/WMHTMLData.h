//
//  WMHTMLData.h
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 28/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


NS_ASSUME_NONNULL_BEGIN

@interface WMHTMLData : NSObject

//NSMutableData *htmlData;                       // data for .html file
//NSUInteger     htmlData_currentPosition_css;   // for insertion
//NSUInteger     htmlData_currentPosition_html;  // for insertion

-(instancetype)init;

- (void)insertCss:(NSString *)cssString;
- (void)insertHtml:(NSString *)htmlString;
- (void)insertHtml:(NSString *)htmlString withAttributes:(NSString *)fontFaceAttributes;
- (void)insertCode:(NSString *)htmlString;  /**< e.g.:    <span class="cppcode">htmlString</span> */
- (void)insertCode:(NSString *)htmlString withClass:(NSString *)classIdentifier; /**< e.g.:  <span class="cppcode"><span class="identifier_class_system">NSString</span></span> */

- (void)insertBR;

- (NSData *)html;    // html
- (NSData *)wp;      // WordPress

- (BOOL)writeHTMLToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile;
- (BOOL)writeWPToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile;

@end

NS_ASSUME_NONNULL_END
