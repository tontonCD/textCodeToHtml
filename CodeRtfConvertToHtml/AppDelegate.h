//
//  AppDelegate.h
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 18/12/2014.
//  Copyright (c) 2014 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <WebKit/WebKit.h>

#import "WMHTMLData.h"




// used for both identificaton and output:
// OLD
//
//#define kCompPlainText          { "plainText",     0,   0,  48 }    // # 00 00 30; added 1.3
//#define kCompComment            { "comment",       0, 116,   0 }
//#define kCompString             { "string",      196,  26,   0 }
//#define kCompCharacters         { "character",    28,   0, 207 }
//#define kCompKeywords           { "keyword",     128,  00, 128 }
//#define kCompInstanceVariables  { "instanceVar",  63, 110, 116 }
//#define kCompMacros             { "macro",       100,  56,  32 }
//#define kCompClassName          { "className",    92,  38, 153 }
//#define kCompFunction           { "function",     46,  13, 110 }
//// console (lldb):
//#define kCompInvite             { "invite",       81, 112, 255 }
//#define kCompCommand            { "command",      81, 112, 255 } // , -> font-weight:bold
//
//#define kNumColors              11








@interface AppDelegate : NSObject <NSApplicationDelegate, NSTableViewDelegate, NSTextViewDelegate>
{

    NSString *aFileNamePath_Input;          // old Name: old Name: aFileInPath;

     // for display: (to be OBSO)
    NSString *aFileNameString_Input;        // old Name: aFileInName;
    NSString *aFileNameString_Html;         // old Name: aHtmlOutName;
    NSString *aFileNameString_WordPress;    // old Name: aWPreOutName;
    
    
    // for read/write:
    NSString *aFileNamePath_Html;           // old Name: aHtmlOutPath;
    NSString *aFileNamePath_WordPress;      // old Name: aWPreOutPath;
    
    // for parse:
    NSData *fileInData;
    NSAttributedString *stringFromFile;
    // optimisation for [stringFromFile string]
    NSString *stringFromFile_String;

    
    WMHTMLData  *aHtmlData;                       // data for .html file -> NOW for both files (todo: rename)
    
//    NSMutableData *aWPreData;   // data for WordPress articles -> now in aHtmlData
}

@property IBOutlet NSTextField  *aEntryLabel;
@property IBOutlet NSTextField  *aResultLabel;

@property IBOutlet NSTextView   *aTextView;

#if true // OLD (deprecated)
// "In apps that run in OS X 10.10 and later, use the WKWebView class instead of using WebView.")
@property IBOutlet WebView      *aWebView;
#else // TODO:
// NOTE: WK_EXTERN API_AVAILABLE(macosx(10.10), ios(8.0))
@property IBOutlet WKWebView    *aWKWebView;
#endif

@property IBOutlet NSTableView  *aAttributesTableView;

@property IBOutlet NSButton     *aOpenButton;
@property IBOutlet NSButton     *aSaveHtmlButton;
@property IBOutlet NSButton     *aSaveWPreButton;

@property IBOutlet NSTextField  *aFileNameTextField_Input;
@property IBOutlet NSTextField  *aFileNameTextField_Html;        // old Name: aHtmlName
@property IBOutlet NSTextField  *aFileNameTextField_WordPress;   // old Name: aWPreName
// TODO
//@property IBOutlet NSTextField  *aFileName_CSS;

-(IBAction)openAction:(id)sender;
-(IBAction)saveHtmlAction:(id)sender;
-(IBAction)saveWPreAction:(id)sender;

@end

