//
//  WMPList.m
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 20/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

#import "WMPList.h"

/*
 * prefs are in /Users/Administrateur/Library/Developer/Xcode/UserData/FontAndColorThemes/,
 *  - Default.dvtcolortheme     <- this one
 *  - Default.xccolortheme
 *  - Dusk.dvtcolortheme
 */

// defines:
static NSString *defaultFile = @"Default.dvtcolortheme";

@implementation WMPList

+(NSDictionary *)getFontsAndColorsFromPrefs   {

    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    
    NSString *defaultFileName =      [defaultFile stringByDeletingPathExtension];   // -> @"Default"
    NSString *defaultFileExtension = [defaultFile pathExtension];                   // -> @"dvtcolortheme"
#pragma unused(defaultFileExtension)
    
    //NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *libPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fontAndColorThemesPath = [libPath stringByAppendingString:@"/Developer/Xcode/UserData/FontAndColorThemes"];
    
    NSString *plistPath = [fontAndColorThemesPath stringByAppendingPathComponent:defaultFile];

    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:defaultFileName ofType:@"xccolortheme"];
    }
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    
    NSDictionary *result = (NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistXML
                                                                    mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                                                                    format:&format
                                                                          errorDescription:&errorDesc];
    
    if (!result) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
    }
    
    return result;
}

@end
