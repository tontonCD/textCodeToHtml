//
//  ColorComponents.m
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 19/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

#import "ColorComponents.h"
#import "WMPList.h"





@interface StyleComponent()
-(BOOL)_isEqualForColour:(StyleComponent *)object;
-(BOOL)_isEqualForFont:(StyleComponent *)object;
@end

@implementation StyleComponent : NSObject

// not really USED
//-(instancetype)initWithColor:(NSColor *)color   {
//    self = [super init];
//    if(self)
//    {
//        self->sStyleName = @"-";
//        
//        float fRed =   color.redComponent   * 2550.0;     // 0
//        float fGreen = color.greenComponent * 2550.0;
//        float fBlue =  color.blueComponent  * 2550.0;     // 0.188235 -> 0.18823499977588654
//        float fAlpha = color.alphaComponent * 2550.0;
//        
//        self->sRed =   (int)lroundf(fRed   );    // 0
//        self->sGreen = (int)lroundf(fGreen );
//        self->sBlue =  (int)lroundf(fBlue  );    // 0.188235 -> 0.18823499977588654
//        self->sAlpha = (int)lroundf(fAlpha );
//        
//    }
//    return self;
//}
-(instancetype)initWithColorString:(NSString *)color andFontString:(NSString *)font    {
    self = [super init];
    if(self)
    {
        self->sShortStyleName = @"-";
        self->sUsed = 0;
        
        NSArray<NSString *> *plainComponents = [color componentsSeparatedByString:@" "];
        float fRed =   [plainComponents[0] floatValue] * 2550.0;    // 0
        float fGreen = [plainComponents[1] floatValue] * 2550.0;
        float fBlue =  [plainComponents[2] floatValue] * 2550.0;    // 0.188235 -> 0.18823499977588654
        float fAlpha = [plainComponents[3] floatValue] * 2550.0;

        self->sRed =   (int)lroundf(fRed   );    // 0
        self->sGreen = (int)lroundf(fGreen );
        self->sBlue =  (int)lroundf(fBlue  );    // 0.188235 -> 0.18823499977588654
        self->sAlpha = (int)lroundf(fAlpha );
        
        // fonts are saved as (e.g.) "Menlo-Regular - 11.0"
        plainComponents = [font componentsSeparatedByString:@" - "];
        self->fontName = plainComponents[0];
        self->fontSize = plainComponents[1];
        self->fontPtSize = [self->fontSize floatValue]; // TODO: CGFloat ?
    }
    return self;
}
-(instancetype)initWithColor:(NSColor *)color andFont:(NSFont *)font    {
    self = [super init];
    if(self)
    {
        self->sShortStyleName = @"-";
        self->sUsed = 0;

        
        float fRed =   color.redComponent   * 2550.0;
        float fGreen = color.greenComponent * 2550.0;
        float fBlue =  color.blueComponent  * 2550.0;
        float fAlpha = color.alphaComponent * 2550.0;

        self->sRed =   (int)lroundf(fRed   );
        self->sGreen = (int)lroundf(fGreen );
        self->sBlue =  (int)lroundf(fBlue  );
        self->sAlpha = (int)lroundf(fAlpha );
        

        self->fontName = font.fontName;
        self->fontPtSize = font.pointSize;
        self->fontSize = [NSString stringWithFormat:@"%1f", font.pointSize]; // OBSO ?
    }
    return self;
}

-(NSString *)setStyleName:(NSString *)styleName {
    self->sShortStyleName = styleName;
    //NSArray<NSString *> *components = [styleName componentsSeparatedByString:@"."];
    self->sShortStyleName_escaped = [self->sShortStyleName stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    return self->sShortStyleName;
}



-(NSString *)getHexString   {
    NSString *result = [[NSString alloc] initWithFormat:@"#" "%02X" "%02X" "%02X" "%02X",
//                        (float)theComponentObject->sRed   / 10.0,
//                        (float)theComponentObject->sGreen / 10.0,
//                        (float)theComponentObject->sBlue  / 10.0,
//                        (float)theComponentObject->alpha  / 10.0
                        self->sRed   / 10,
                        self->sGreen / 10,
                        self->sBlue  / 10,
                        self->sAlpha / 10
                        ];
    return result;
}
-(NSString *)getStyleName   {
    return sShortStyleName;
}
-(NSString *)getEscapedStyleName   {
    return sShortStyleName_escaped;
}


-(NSString *)description    {
    NSString *result = [NSString stringWithFormat:@"{ name: %@\n  color: %i, %i, %i, %i\n  font: %@ - %@ }",
                        sShortStyleName,
                        sRed, sGreen, sBlue, sAlpha,
                        fontName, fontSize];
    return result;
}
-(BOOL)_isEqualForColour:(StyleComponent *)object   {
    BOOL result = (    (self->sRed ==   object->sRed  )
                   &&  (self->sGreen == object->sGreen)
                   &&  (self->sBlue ==  object->sBlue )
                   &&  (self->sAlpha == object->sAlpha)   );
    return result;
}
-(BOOL)_isEqualForFont:(StyleComponent *)object   {
    BOOL result = (    ([self->fontName isEqualToString:object->fontName] )
                   &&  ( self->fontPtSize == object->fontPtSize           )   );
    return result;
}
-(BOOL)isEqual:(StyleComponent *)object   {
    BOOL result = (    [self _isEqualForColour:object]
                   &&  [self _isEqualForFont:  object]    );
    return result;
}
@end


@interface ColorComponents()
{
}

@end


@implementation ColorComponents



static NSMutableArray<StyleComponent *> *stylesArray;


+ (NSMutableArray<StyleComponent *> *)stylesArray       {
    return stylesArray;
}


-(instancetype)init {
    self = [super init];
    return self;
}
-(instancetype)initWithName:(NSString *)name andColor:(NSColor *)color {
    self = [super init];
    if(self)
    {
        self->name = name;
        self->components = color;
    }
    return self;
}
-(instancetype)initWithFont:(NSFont *)font andColor:(NSColor *)color    {
    self = [super init];
    if(self)
    {
        self->name = [font fontName];
        self->components = color;
    }
    return self;
}

+(void)initFromPrefs    {
    NSDictionary *dico = [WMPList getFontsAndColorsFromPrefs];
    
    NSDictionary *colorsDico0 = [dico objectForKey:@"DVTSourceTextSyntaxColors"];
    NSDictionary *fontsDico0 =  [dico objectForKey:@"DVTSourceTextSyntaxFonts"];
    
    stylesArray =    [[NSMutableArray alloc] init];

    for(NSString *keyString in colorsDico0.allKeys)  {
        NSString *colorStringValue = [colorsDico0 objectForKey:keyString];
        NSString *fontStringValue =  [fontsDico0  objectForKey:keyString];
        
        /* shorten the key, e.g.
         *  @"xcode.syntax.plain"                       =>  @"plain"
         *  @"xcode.syntax.identifier.variable.system"  =>  @"identifier.variable.system" */
        NSString *shortKeyString = [keyString substringFromIndex:13];
        
        StyleComponent *styleComponentValue = [[StyleComponent alloc] initWithColorString:colorStringValue andFontString:fontStringValue];

        
        [styleComponentValue setStyleName:shortKeyString];
        [stylesArray addObject:    styleComponentValue];

    }
    
    // make enumeration for TableView
    for(NSUInteger styleIndex = 0; styleIndex<stylesArray.count; styleIndex++)    {
        stylesArray[styleIndex]->sStyleIndex = styleIndex;
    }
    
}


+(StyleComponent *)findStyleComponentFromStyle:(StyleComponent *)styleComponent  {
    for(NSUInteger fontIndex = 0; fontIndex<stylesArray.count; fontIndex++)  {
        if([styleComponent isEqual:stylesArray[fontIndex]])
        {
            return stylesArray[fontIndex];
        }
    }
    return nil;
}
@end


