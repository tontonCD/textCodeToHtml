//
//  WMPList.h
//  CodeRtfConvertToHtml
//
//  Created by Christophe on 20/08/2020.
//  Copyright © 2020 Christophe. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WMPList : NSObject

+(NSDictionary *)getFontsAndColorsFromPrefs;

@end

NS_ASSUME_NONNULL_END
