Version Française [ici](ReadMe.FR.md).  
Project URL: [https://gitlab.com/tontonCD/textCodeToHtml]()

PROJECT
=======
**textCodeToHtml** makes you able to obtain, from .rtf or .doc files, new files in a format attempted to be convenient for a Web publication.
This is purposed for texts containing **source code** (in a programming language).
As the files are completely defined while the generation completes, the method is well more efficient than if you used a plugin, because it would have perform the same thing on the fly.

These files are generated:

- html file (.html): you can publish it directly using an ftp client,
- Wordpress (.txt): the software builds primally a CSS types description (that you would insert in your Appearance -> Theme Options), then a text file containing the corresponding css Tags that Wordpress would handle,


 
HOW TO
===============

How to:

1) open a new .rtf or .doc file, type your text, copy/paste any code from Xcode,



2) the "main" text is identified with strict black-color (color: #000000). Only non-black *colors* would be checked as Code-Style parts, so avoid to define some Code-Style as black (@see  VERSION: #1.4 note)


3) launch the software, open the file (with the "Open" button) ; any color that can be recognized makes the generation of an html tag, a .html file is build, as a .txt file for Wordpress (it handles differently the line feeds)


VERSION: #1.8
===============
added fontFace attributes : bold/italic,


VERSION: #1.6
===============
highlight the Style TableView for currently identified Style (in real time),


VERSION: #1.5
===============
Allows alpha (transparency) in CSS colors,


VERSION: #1.4
===============
the styles attributes will be read from the true Xcode Prefs. Checked with Xcode 10.1.


VERSION: #1.3
===============
Assigning a code type to a color tag is still hard coded, so you may compile the code yourself for personalization. 
Don't claim about the dirty presentation, because the project is also an example for the way it can evolve.  

- FIX (features): implements the open file chooser (the file name is no more hard coded),
- FIX (features): solved the display issue for the Web View and the tag Table,

Please have a look at CodeRtfConvert_log.rtf for more details about versions (correctly displayed within Xcode).

TODO
====
- look for Bold and Italic styles,
- look for text notes, handle the numeration,
- look for comments (`/* .. */` -> `<!-- ... -->`) ?,
- build separately .css and .html files
- read the .css from the parent folder of the input file, if any (actually, the values used for match while parsing the input file are defined by code from the project),
- purpose when encounters an unknown style while parsing the input file, that you can assign it a style name,

EXAMPLE
=======
*(TODO)*

NOTE
====
Some fine tool you can also use, is the "textutil" command line with the "-convert html" option, so sad it doesn't take care about code formatting.

---
Thanks for visit the project home page: [http://chdfree.free.fr/wanmore/index.php/textcodetohtml/](http://chdfree.free.fr/wanmore/index.php/textcodetohtml/) 
